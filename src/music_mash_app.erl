%%%-------------------------------------------------------------------
%%% @author Zandra Norman <zandra.norman@gmail.com>
%%% @doc music_mash public API
%%%     A REST API to get description and album information based on
%%%     a music brainz id.
%%% @end
%%%-------------------------------------------------------------------

-module(music_mash_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
    start_cowboy(),
    music_mash_sup:start_link().

start_cowboy() ->
    Dispatch = cowboy_router:compile([
        {'_', [{"/artist/:mbid", music_mash_handler, []}]}
    ]),
    {ok, _} = cowboy:start_http(my_http_listener, 100,
        [{port, 8080}],
        [{env, [{dispatch, Dispatch}]}]
    ).

%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
