%%%-------------------------------------------------------------------
%%% @author Zandra Norman <zandra.norman@gmail.com>
%%% @doc
%%%     The request handler that retrievs and returns the right
%%%     information from music brainz, wikipedia and cover atrs.
%%% @end
%%%-------------------------------------------------------------------
-module(music_mash_handler).

% Cowboy callbacks
-export([init/3, allowed_methods/2, content_types_provided/2, to_json/2,
         resource_exists/2]).

-define(USERAGENT, "Music Mash/0.1.0 (zandra.norman@gmail.com)").
-define(MUSIC_BRAINZ_URL(MBID),
        "https://musicbrainz.org/ws/2/artist/" ++ MBID ++
        "?fmt=json&inc=url-rels+release-groups").
-define(EXTRACT_WIKI_TITLE(String), String -- "https://en.wikipedia.org/wiki/").
-define(WIKI_URL(Title),
        "https://en.wikipedia.org/w/api.php?action=query&format=json&prop=extracts"
        "&exintro=true&redirects=true&titles=" ++ Title).
-define(COVER_ART_URL(Id), "http://coverartarchive.org/release-group/" ++ Id).

init(_, _Req, _State) ->
    {upgrade, protocol, cowboy_rest}.

%% Only allow GET methods
allowed_methods(Req, State) ->
    {[<<"GET">>], Req, State}.

%% Provide json only
content_types_provided(Req, State) ->
    {[{<<"application/json">>, to_json}], Req, State}.

%% Check that the music brainz id points to an artist, and add
%% it to the state.
resource_exists(Req, State) ->
    {MBID, _} = cowboy_req:binding(mbid, Req),
    MBBody = get_music_brainz_body(binary_to_list(MBID)),
    {true, Req, [MBID, MBBody|State]}.

%% Get the remaining information from wikipedia and cover arts archive.
to_json(Req, [MBID, MBBody|_] = State) ->
    MBMap = jiffy:decode(MBBody, [return_maps]),
    Self = self(),
    WikiPid = spawn_link(fun() -> wiki(Self, MBMap) end),
    Albums0 = get_albums(MBMap),
    AlbumPids = [spawn_link(fun() -> add_album_cover(Self, Album) end)
                 || Album <- Albums0],
    [Description] = await([WikiPid]),
    Albums = await(AlbumPids),
    Result = #{<<"mbid">> => MBID,
               <<"description">> => Description,
               <<"albums">> => Albums},
    {jiffy:encode(Result), Req, State}.


%%%-------------------------------------------------------------------
%%% Internal functions
%%%-------------------------------------------------------------------

await(Pids) ->
    await(Pids, []).

await([], Answers) ->
    Answers;
await([Pid|Pids], Answers) ->
    receive
        {Pid, Answer} ->
            await(Pids, [Answer|Answers])
    end.

%% Get artist information from music brainz
get_music_brainz_body(MBID) ->
    RequestHeaders = [{"user-agent", ?USERAGENT}],
    URL = ?MUSIC_BRAINZ_URL(MBID),
    request(URL, RequestHeaders).

%% Function for the process that queries wikipedia information.
wiki(Parent, MBMap) ->
    WikiTitle = get_wiki_title(MBMap),
    Description = get_description(WikiTitle),
    Parent ! {self(), Description}.

get_wiki_title(MBMap) ->
    Relations = maps:get(<<"relations">>, MBMap),
    WikiMap = hd(lists:dropwhile(fun(X) ->
                                     maps:get(<<"type">>, X) =/= <<"wikipedia">>
                                 end, Relations)),
    WikiURLInfo = maps:get(<<"url">>, WikiMap),
    WikiURL = maps:get(<<"resource">>, WikiURLInfo),
    WikiURLString = binary_to_list(WikiURL),
    ?EXTRACT_WIKI_TITLE(WikiURLString).

get_description(Title) ->
    URL = ?WIKI_URL(Title),
    WikiBody = request(URL),
    WikiMap = jiffy:decode(WikiBody, [return_maps]),
    Query = maps:get(<<"query">>, WikiMap),
    Pages = maps:get(<<"pages">>, Query),
    [Page|_] = maps:values(Pages),
    maps:get(<<"extract">>, Page).

%% Function for the process requesting album image information from cover
%% arts archive.
add_album_cover(Parent, #{<<"id">> := ID} = Album) ->
    URL = ?COVER_ART_URL(binary_to_list(ID)),
    case request(URL) of
        {error, not_found} ->
            Parent ! {self(), Album};
        CoverBody ->
            Image = get_cover_image(CoverBody),
            Parent ! {self(), Album#{<<"image">> => Image}}
    end.

get_cover_image(CoverBody) ->
    CoverMap = jiffy:decode(CoverBody, [return_maps]),
    ImageMaps = maps:get(<<"images">>, CoverMap),
    [ImageMap] = lists:filter(fun(Map) ->
                                  maps:get(<<"front">>, Map)
                              end, ImageMaps),
    maps:get(<<"image">>, ImageMap).

get_albums(MBMap) ->
    ReleaseGroups = maps:get(<<"release-groups">>, MBMap),
    FilterMapFun = fun(Rel) ->
                       {maps:get(<<"primary-type">>, Rel) == <<"Album">>,
                        #{<<"id">> => maps:get(<<"id">>, Rel),
                          <<"title">> => maps:get(<<"title">>, Rel)}}
                   end,
    lists:filtermap(FilterMapFun, ReleaseGroups).

request(URL) ->
    request(URL, []).

request(URL, RequestHeader) ->
    case lhttpc:request(URL, get, RequestHeader, 5000) of
        {ok, {{200, "OK"}, _, Body}} ->
            Body;
        {ok, {{307, _}, Header, _}} ->
            NewURL = proplists:get_value("location", Header),
            request(NewURL, RequestHeader);
        {ok, {{302, _}, Header, _}} ->
            NewURL = proplists:get_value("location", Header),
            request(NewURL, RequestHeader);
        {_, {{404, _}, _, _}} ->
            {error, not_found}
    end.

