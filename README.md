music_mash
=====

A REST API that gives you artist and album information based on a
MusicBrainz ID passed in.

Build
-----

    $ rebar3 release

Run
-----

    $ _build/default/rel/music_mash/bin/music_mash start

